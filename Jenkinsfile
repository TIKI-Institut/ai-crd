pipeline {
    agent {
        kubernetes {
            defaultContainer 'release'
            yamlMergeStrategy merge()
            inheritFrom 'dsp-release'
        }
    }

    options {
        buildDiscarder(logRotator(daysToKeepStr: '30'))
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

	// Build container via docker because docker-compose can not correctly mount working dir
    stages {
        stage('Build Docker CICD env') {
            steps {
                sh '''docker build --force-rm --target jenkins -t ai-crd-cicd .'''
            }
        }

        stage('Test') {
            steps {
				sh '''docker run -w /app/ ai-crd-cicd:latest make test'''
            }
        }
    }

    post {
        failure {
            bitbucketStatusNotify(buildState: 'FAILED')
        }
        fixed {
            notifyViaSlack("#00FF00")
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
    }
}

def notifyViaSlack(String colorCode) {
    def subject = "${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} ${env.BUILD_URL}"

    slackSend(color: colorCode, message: summary)
}
