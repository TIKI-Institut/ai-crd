package jsonSchema

import (
	"github.com/alecthomas/jsonschema"
	"net/url"
	"reflect"
)

// NewJSONSchema generates a JSON schema according to http://json-schema.org/draft-04/schema# from jsonschema tags of the provided obj.
// - Additional Properties are not allowed. This enables JSON with values provided in the wrong format for optional fields to be identified.
// - If nil should be accepted as a valid input for a field (e.g. type pointer to struct or slice) the field must be tagged with `jsonschema:nullable`. Otherwise, the schema will reject null as invalid type.
// - The schema does not inline embed structs. This is enabled so the JSON schema can be used with yaml.Marshal/Unmarshal.
// - A typeMapper is optional to alter how basic/custom go types are represented in the schema. Note that typeMapper does not override a schema element that is provided with a field tag.
func NewJSONSchema(obj interface{}, typeMapper func(reflect.Type) *jsonschema.Type) *jsonschema.Schema {
	targetType := reflect.TypeOf(obj)

	additionalFields := func(r reflect.Type) []reflect.StructField {
		//inject "$schema" field to top-level object
		if r == targetType {
			return []reflect.StructField{
				{
					Name: "$schema",
					Tag:  `json:"$schema,omitempty" jsonschema:"title=target schema"`,
					Type: reflect.TypeOf(url.URL{}),
				},
			}
		}
		return nil
	}

	reflector := &jsonschema.Reflector{
		AllowAdditionalProperties: false,
		YAMLEmbeddedStructs:       true,
		TypeMapper:                typeMapper,
		AdditionalFields:          additionalFields,
	}
	return reflector.ReflectFromType(targetType)
}
