package jsonSchema

import (
	"encoding/json"
	"fmt"
	"github.com/alecthomas/jsonschema"
	"github.com/pkg/errors"
	"github.com/xeipuuv/gojsonschema"
	"strings"
)

func ValidateJSONUsingSchema(bytes []byte, schema *jsonschema.Schema) error {

	if len(bytes) == 0 {
		return nil
	}

	schemaBytes, err := json.Marshal(schema)
	if err != nil {
		return err
	}
	schemaLoader := gojsonschema.NewBytesLoader(schemaBytes)
	documentLoader := gojsonschema.NewBytesLoader(bytes)

	if validationResult, err := gojsonschema.Validate(schemaLoader, documentLoader); err != nil {
		return errors.Wrap(err, "unable to validate json schema")
	} else if !validationResult.Valid() {
		sBuilder := strings.Builder{}

		for _, v := range validationResult.Errors() {
			sBuilder.WriteString(fmt.Sprintf("in %s : %s\n", v.Context().String(), v.Description()))
		}

		return fmt.Errorf("json data does not satisfy schema:\n%s", sBuilder.String())
	} else {
		return nil
	}
}
