package jsonSchema

import "github.com/alecthomas/jsonschema"

func MergeSchemas(rootSchema *jsonschema.Schema, schemasToMerge []*jsonschema.Schema, overwrite bool) {

	for _, schemaToMerge := range schemasToMerge {

		for key, def := range schemaToMerge.Definitions {
			if _, found := rootSchema.Definitions[key]; !found || overwrite {
				rootSchema.Definitions[key] = def
			}
		}
	}
}
