package v1alpha1

import (
	"bitbucket.org/TIKI-Institut/ai-crd/api/v1alpha1"
	jsonSchema2 "bitbucket.org/TIKI-Institut/ai-crd/jsonSchema"
	"github.com/alecthomas/jsonschema"
)

func PrincipalAppSchema() *jsonschema.Schema {
	rootSchema := jsonSchema2.NewJSONSchema(&v1alpha1.PrincipalAppConfiguration{}, nil)

	//Create the Json schemas of all PrincipalAppOpenActions
	actionSchemas := []*jsonschema.Schema{jsonSchema2.NewJSONSchema(&v1alpha1.OpenLink{}, nil),
		jsonSchema2.NewJSONSchema(&v1alpha1.OpenComponent{}, nil)}

	//Create the Json schemas of all PrincipalAppAccessControls
	accessControlSchemas := []*jsonschema.Schema{jsonSchema2.NewJSONSchema(&v1alpha1.RequiresRole{}, nil),
		jsonSchema2.NewJSONSchema(&v1alpha1.OpenComponent{}, nil)}

	//merge schemas (do not overwrite existing definitions)
	jsonSchema2.MergeSchemas(rootSchema, append(actionSchemas, accessControlSchemas...), false)

	return rootSchema
}
