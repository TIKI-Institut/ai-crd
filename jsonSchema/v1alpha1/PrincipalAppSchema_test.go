package v1alpha1

import (
	"encoding/json"
	"os"
	"testing"

	"bitbucket.org/TIKI-Institut/ai-crd/api/v1alpha1"
	"bitbucket.org/TIKI-Institut/ai-crd/jsonSchema"
	"github.com/stretchr/testify/assert"
)

func TestPrincipalAppSchema(t *testing.T) {
	t.Run("schema creation", func(t *testing.T) {
		schema := PrincipalAppSchema()
		assert.NotNil(t, schema)

		enc := json.NewEncoder(os.Stdout)
		enc.SetIndent("", "  ")
		enc.Encode(schema)
	})

	t.Run("valid (containing openLink)", func(t *testing.T) {
		valid := `{"accessControl":{"requiresRole":{"clientId":"digitalization","role":"kubernetes-status"}},"image":"workloads.png","key":"workloads","name":"Workloads","openAction":{"openComponent":{"config":{"workloadBackendUrl":"https://kubernetes-status.tiki-dsp.tech/api"},"name":"workloads"}}}`
		err := jsonSchema.ValidateJSONUsingSchema([]byte(valid), PrincipalAppSchema())
		assert.NoError(t, err)
	})
	t.Run("valid (containing openComponent action)", func(t *testing.T) {
		valid := `{"key":"spark-history","name":"Spark History","image":"spark-history.png","openAction":{"openComponent":{"name":"spark"}},"accessControl":{"requiresRole":{"clientId":"spark-history","role":"open"}}}`
		err := jsonSchema.ValidateJSONUsingSchema([]byte(valid), PrincipalAppSchema())
		assert.NoError(t, err)
	})
	t.Run("valid (without optional attributes)", func(t *testing.T) {
		valid := `{"key":"spark-history","name":"Spark History","image":"spark-history.png","openAction":{"openComponent":{"name":"blub"}}}`
		err := jsonSchema.ValidateJSONUsingSchema([]byte(valid), PrincipalAppSchema())
		assert.NoError(t, err)
	})
	t.Run("invalid (missing openaction", func(t *testing.T) {
		invalid := `{"key":"spark-history","name":"Spark History","image":"spark-history.png","accessControl":{"requiresRole":{"clientId":"spark-history","role":"open"}}}`
		err := jsonSchema.ValidateJSONUsingSchema([]byte(invalid), PrincipalAppSchema())
		assert.EqualError(t, err, "json data does not satisfy schema:\nin (root) : openAction is required\n")
	})
}

func TestJSONMarshalling_App(t *testing.T) {
	app := v1alpha1.PrincipalAppConfiguration{
		Key:           "spark-history",
		Name:          "Spark History",
		Image:         "spark-history.png",
		OpenAction:    v1alpha1.PrincipalAppOpenAction{OpenLinkAction: &v1alpha1.OpenLink{Url: "https://spark-history.tiki-test.tiki-dsp.io"}},
		AccessControl: &v1alpha1.PrincipalAppAccessControl{RequiresRole: &v1alpha1.RequiresRole{ClientId: "spark-history", Role: "open"}},
	}

	data, err := json.Marshal(&app)

	assert.NoError(t, err)
	assert.EqualValues(t, `{"key":"spark-history","name":"Spark History","image":"spark-history.png","openAction":{"openLink":{"url":"https://spark-history.tiki-test.tiki-dsp.io"}},"accessControl":{"requiresRole":{"clientId":"spark-history","role":"open"}}}`, string(data))
}
