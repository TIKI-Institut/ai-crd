/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PrincipalAppSpec defines the desired state of PrincipalApp
type PrincipalAppSpec struct {
	//PrincipalApp contains configuration for principal application
	PrincipalApp PrincipalAppConfiguration `json:"principalApp"`
}

// PrincipalAppStatus defines the observed state of PrincipalApp
type PrincipalAppStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// PrincipalApp is the Schema for the principalapps API
type PrincipalApp struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PrincipalAppSpec   `json:"spec,omitempty"`
	Status PrincipalAppStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PrincipalAppList contains a list of PrincipalApp
type PrincipalAppList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PrincipalApp `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PrincipalApp{}, &PrincipalAppList{})
}

// PrincipalAppConfiguration represents the whole config of a principal app
// This config is for example used in the principal dashboard
type PrincipalAppConfiguration struct {
	//the apps id
	Key string `json:"key" jsonschema:"description=the apps id"`
	//the apps display name
	Name string `json:"name" jsonschema:"description=the apps display name"`
	//url to the image displayed for this app
	Image         string                     `json:"image,omitempty" jsonschema:"description=url to the image displayed for this app"`
	OpenAction    PrincipalAppOpenAction     `json:"openAction"`
	AccessControl *PrincipalAppAccessControl `json:"accessControl,omitempty"`
}

// PrincipalAppOpenAction represents the way a principal app will be opened
// Only one of its fields may be set.
// The following kubebuilder commands ensure, that at most one of the properties can be set
// +kubebuilder:validation:MinProperties:=1
// +kubebuilder:validation:MaxProperties:=1
type PrincipalAppOpenAction struct {
	//configuration for opening a ui link
	OpenLinkAction *OpenLink `json:"openLink,omitempty" jsonschema:"oneof_required=openComponent,description=configuration for opening a link"`
	//configuration for opening a ui component
	OpenComponentAction *OpenComponent `json:"openComponent,omitempty" jsonschema:"oneof_required=openLink,description=configuration for opening a ui component"`
}

/* open link */

type OpenLink struct {
	//url to the external app to be opened
	Url string `json:"url" jsonschema:"description=url to the external app to be opened"`
}

/* open component */

type OpenComponent struct {
	//the ui component name which shall be opened
	Name string `json:"name" jsonschema:"description=the ui component name which shall be opened"`
	//a configuration object for this component
	Config map[string]string `json:"config,omitempty" jsonschema:"description=a configuration object for this component"`
}

// PrincipalAppAccessControl represents the way a principal app can be accessed
// Only one of its fields may be set (actually only one field)
// The following kubebuilder commands ensure, that at most one of the properties can be set
// +kubebuilder:validation:MinProperties:=1
// +kubebuilder:validation:MaxProperties:=1
type PrincipalAppAccessControl struct {
	//configuration for required roles
	RequiresRole *RequiresRole `json:"requiresRole,omitempty" jsonschema:"description=configuration for required roles"`
}

/* requires role */

type RequiresRole struct {
	//client id which will be checked for an existing role
	ClientId string `json:"clientId" jsonschema:"description=client id which will be checked for an existing role"`
	//the needed role
	Role string `json:"role" jsonschema:"description=the needed role"`
}
