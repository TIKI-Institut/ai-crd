/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PrincipalFilesystemMountSpec defines the desired state of PrincipalFilesystemMount
type PrincipalFilesystemMountSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Name is the name of the mounted filesystem; it will appear as root folder in your API
	Name string `json:"name,omitempty"`

	MountConfig PrincipalFilesystemMountConfig `json:"mountConfig,omitempty"`
}

// +kubebuilder:validation:MinProperties:=1
// +kubebuilder:validation:MaxProperties:=1
type PrincipalFilesystemMountConfig struct {
	S3       *S3       `json:"s3,omitempty" jsonschema:"oneof_required=s3,description=configuration for a s3 mount"`
	HttpRest *HttpRest `json:"httpRest,omitempty" jsonschema:"oneof_required=httpRest,description=configuration for a http rest served fs mount"`
	Hdfs     *Hdfs     `json:"hdfs,omitempty" jsonschema:"oneof_required=hdfs,description=configuration for a hdfs fs mount"`
	Smb      *Smb      `json:"smb,omitempty" jsonschema:"oneof_required=smb,description=configuration for a samba fs mount"`
}

type S3 struct {
	AccessKeySecretRef    coreV1.SecretKeySelector `json:"accessKeySecretRef,omitempty" jsonschema:"description=location of login accessKey"`
	AccessSecretSecretRef coreV1.SecretKeySelector `json:"accessSecretSecretRef,omitempty" jsonschema:"description=location of login accessSecret"`
	Region                string                   `json:"region,omitempty" jsonschema:"description=region of the S3 server"`
	Endpoint              string                   `json:"endpoint,omitempty" jsonschema:"description=S3 server endpoint"`
	DisableSSL            bool                     `json:"disableSSL,omitempty" jsonschema:"description=should the connection be established using SSL"`
	ForcePathStyle        bool                     `json:"forcePathStyle,omitempty" jsonschema:"description=active S3 path style"`
	Bucket                string                   `json:"bucket,omitempty" jsonschema:"description=the bucket which shall be mounted"`
	BasePath              string                   `json:"basePath,omitempty" jsonschema:"description=the base path which shall be mounted"`
}

type HttpRest struct {
	Endpoint string `json:"endpoint,omitempty" jsonschema:"description=Http rest server endpoint"`
}

type Hdfs struct {
	HdfsBaseDirectory       string                      `json:"hdfsBaseDirectory,omitempty" jsonschema:"description=the base path which shall be mounted"`
	KrbRealm                string                      `json:"krbRealm,omitempty" jsonschema:"description=the kerberos realm"`
	KrbUserPrincipal        string                      `json:"krbUserPrincipal,omitempty" jsonschema:"description=the kerberos user principal"`
	KrbServicePrincipleName string                      `json:"krbServicePrincipleName,omitempty" jsonschema:"description=the kerberos service principal"`
	KeytabSecretRef         coreV1.SecretKeySelector    `json:"keytabSecretRef,omitempty" jsonschema:"description=the keytab secret"`
	KrbConfigRef            coreV1.ConfigMapKeySelector `json:"krbConfigRef,omitempty" jsonschema:"description=the kerberos configmap"`
	HadoopConfig            coreV1.LocalObjectReference `json:"hadoopConfig,omitempty" jsonschema:"description=the hadoop configmap"`
}

// +kubebuilder:validation:MinProperties:=1
// +kubebuilder:validation:MaxProperties:=1
type SmbAuthConfig struct {
	UserPassword *SmbAuthConfigUsernamePassword `json:"userPassword,omitempty" jsonschema:"oneof_required=userPassword,description=auth configuration for a username/password login"`
}

type SmbAuthConfigUsernamePassword struct {
	UsernameSecretRef coreV1.SecretKeySelector `json:"usernameSecretRef,omitempty" jsonschema:"description=samba login username"`
	PasswordSecretRef coreV1.SecretKeySelector `json:"passwordSecretRef,omitempty" jsonschema:"description=samba login password"`
}

type Smb struct {
	Share    string        `json:"share,omitempty" jsonschema:"description=the share name which shall be mounted"`
	BasePath string        `json:"basePath,omitempty" jsonschema:"description=the base path which shall be mounted within the share"`
	Server   string        `json:"server,omitempty" jsonschema:"description=the samba server"`
	Auth     SmbAuthConfig `json:"auth,omitempty" jsonschema:"description=authentication config"`
}

// PrincipalFilesystemMountStatus defines the observed state of PrincipalFilesystemMount
type PrincipalFilesystemMountStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// PrincipalFilesystemMount is the Schema for the principalfilesystemmounts API
type PrincipalFilesystemMount struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PrincipalFilesystemMountSpec   `json:"spec,omitempty"`
	Status PrincipalFilesystemMountStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PrincipalFilesystemMountList contains a list of PrincipalFilesystemMount
type PrincipalFilesystemMountList struct {
	metaV1.TypeMeta `json:",inline"`
	metaV1.ListMeta `json:"metadata,omitempty"`
	Items           []PrincipalFilesystemMount `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PrincipalFilesystemMount{}, &PrincipalFilesystemMountList{})
}
